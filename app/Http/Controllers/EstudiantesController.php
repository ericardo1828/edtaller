<?php

namespace App\Http\Controllers;

use App\estudiantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantes = Estudiantes::all();
        return $estudiantes;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request = request()->all();
        // $estudiante = Estudiantes::create($request);
        // return $estudiante;


        // $data = request(); 
        // DB::table('estudiantes')->insert([
        //     'name' => $data['name'],
        //     'active' => $data['active']
        // ]);
        
       return dd($request);
        
       //$estudiante = 
       Estudiantes::insert($request);
       //return $estudiante;


        // foreach ($request->json as $param => $paramdata) 
        // {
        //     $estudiante = Estudiantes::updateOrCreate(
        //                         ['name' => $paramdata['name']], 
        //                         ['active' => $paramdata['active']], 
        //                         $paramdata
        //                         );
        // }

        //return response()->json(['message' => 'Success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function show(estudiantes $estudiantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function edit(estudiantes $estudiantes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estudiantes $estudiantes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function destroy(estudiantes $estudiantes)
    {
        //
    }
}
